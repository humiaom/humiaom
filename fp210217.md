# Fight Against Myphone

---
2021年更新

## mvp原型图迭代

[![y2BkvT.png](https://s3.ax1x.com/2021/02/17/y2BkvT.png)](https://imgchr.com/i/y2BkvT)

- 删除每周图例
- 新增总计时间
- 新增负债概念
   - 手机使用时长 > 专注时长，进入时间负债
   - 手机使用时长 < 专注时长，进入时间盈余

## 目前进展
[![y2BGrD.md.png](https://s3.ax1x.com/2021/02/17/y2BGrD.md.png)](https://imgchr.com/i/y2BGrD)
[![y2BtVH.md.png](https://s3.ax1x.com/2021/02/17/y2BtVH.md.png)](https://imgchr.com/i/y2BtVH)

---
2020年记录

## mvp原型图

[![BVQhPH.png](https://s1.ax1x.com/2020/10/24/BVQhPH.png)](https://imgchr.com/i/BVQhPH)

## 目前进展

生成每周使用时间柱状图：
![](https://imgchr.com/i/BsYCvj)

生成所有时间的折线图，用来观看趋势：
![](https://imgchr.com/i/BsYiKs)


## 构思

每天都觉得时间不够用<br/>
但是看看手机屏幕使用时长——5、6、7、8...小时<br/>
一周能累积到50小时，也就是说，看手机看了两天？<br/>
哪来的这么多时间玩手机？<br/>

手机，注意力和闲暇时间的竞争者<br/>

我试图对抗它：<br/>
```
每天学习、专注x时间，可以玩x时间的手机
或者玩x时间的手机，必须有学习、专注x时间
```

我将它记录下来，希望有一天能在对抗中<br/>
获得压倒性的胜利<br/>
不再被一台手机绑架<br/>

## 数据来源
- 所有数据记录在gitlab的issue中
   - 手机屏幕使用时间数据来自 iphone自动统计
   - 专注时间使用手机app记录

## 迭代点

- 在网页生成，可查看历史
- 输入手机号或某个特殊编码，自动获取手机屏幕使用时长
- 数据对比
   - 今天和过去的一天
   - 本周和过去的一周

## 开发日记

> d0
- mvp构思
- 迭代构思

> d1 初代码
参考bp，决定在gilab上新建一个仓库，使用issue页面，统计每周的时间，首先建立api连接，获取issue数据，分析数据之后，决定如何获取关键参数

```
# coding = utf - 8

import requests
import pprint
pp = pprint.PrettyPrinter(indent=4)

with open('token.txt') as f:
    pt = f.read() 
    
r = requests.get('https://gitlab.com/api/v4/projects/21987874/issues/1/notes', headers = {"PRIVATE-TOKEN": pt})
# print(r.status_code) # response200，连接成功

describe = r.json()
# pp.pprint(describe)

infomation = []
for i in describe:
    infomation.append(i['body'])
    
newinfomation = [f for f in infomation if 'changed the description' not in f]  
pp.pprint(newinfomation)
```

获取到的初始数据是：
```
[   '## 201018-201024\n\n- 总屏幕使用时间：55小时57分钟\n- 总专注时间：8小时26分钟',
    '## 201011-201017\n\n- 总屏幕使用时间：60小时\n- 总专注时间：7小时']
```

我需要的数据是：
- 标题，即日期，如201018-201024
- y轴数据，即两个时间

参考
- [正则匹配](https://www.cnblogs.com/ZhangHT97/p/13427325.html)
- [findall函数的用法](https://www.cnblogs.com/springionic/p/11327187.html)
寻找规律提取数据的方法。

在此期间发现，在gitlab自己编辑页面的时候，需要考虑到提取问题，设计一些利于标记的符号，且不容易混淆。
比如，本来在记录的日期之间使用-，又使用-（markdown）记录时间，提取时就不太方便。

修改原始数据的记录规则之后，通过正则匹配，获取到日期，时间数据，代码：
```
# coding = utf - 8

import requests # 用于获取api数据
import re # 用于正则匹配，获取特定数据

import pprint
pp = pprint.PrettyPrinter(indent=4)

with open('token.txt') as f:
    pt = f.read() 
    
r = requests.get('https://gitlab.com/api/v4/projects/21987874/issues/1/notes', headers = {"PRIVATE-TOKEN": pt})
# print(r.status_code) # response200，连接成功

describe = r.json()
# pp.pprint(describe)

infomation = []
for i in describe:
    infomation.append(i['body'])
    
newinfomation = [f for f in infomation if 'changed the description' not in f]  
# pp.pprint(newinfomation)

num = len(newinfomation)
# print(num)

for i in newinfomation[0: num]:
    # print(i)
    
    day = re.compile('## (.+)：')
    result1 = day.findall(i)
    
    time = re.compile('时间：(.+),')   
    result2 = time.findall(i)
    list = result1 + result2
    print(list)
```

打印结果如下：

```
['201018~201024', '55小时57分钟', '8小时26分钟']
['201011~201017', '60小时3分钟', '7小时14分钟']
```

数据提取完成，下一步，以list[0]为日记，list[1]和[2]为y轴坐标值，就可以绘图了。

绘图过程中，x小时x分钟的格式影响成型，重新做数据整理，再次通过正则匹配取得时间，计算为分钟数，再组成列表，新的代码为：
```
# coding = utf - 8

import requests # 用于获取api数据
import re # 用于正则匹配，获取特定数据

import pprint
pp = pprint.PrettyPrinter(indent=4)

with open('token.txt') as f:
    pt = f.read() 
    
r = requests.get('https://gitlab.com/api/v4/projects/21987874/issues/1/notes', headers = {"PRIVATE-TOKEN": pt})
# print(r.status_code) # response200，连接成功

describe = r.json()
# pp.pprint(describe)

infomation = []
for i in describe:
    infomation.append(i['body'])
    
newinfomation = [f for f in infomation if 'changed the description' not in f]  
# pp.pprint(newinfomation)

num = len(newinfomation)
# print(num)

for i in newinfomation[0: num]:
    # print(i)
    
    day = re.compile('## (.+)：')
    result1 = day.findall(i)
    
    hour = re.compile('共(.+)小时') 
    result2 = hour.findall(i)
    
    mintues = re.compile('小时(.+)分钟') 
    result3 = mintues.findall(i)
    
    t1 = result2 + result3
    
    t2 = int(t1[0]) * 60 + int(t1[2])
    t3 = int(t1[1]) * 60 + int(t1[3])
    
    timelist =[t2, t3]
    list = [result1, timelist]
    print(list)
```

打印结果：

```
[['201018~201024'], [3357, 506]]
[['201011~201017'], [3603, 434]]
```

这样取值就很方便了，图形title是'201018~201024'，y轴的两个值是[3357, 506]（分钟）

使用柱状图绘制：

```
# coding = utf - 8

import matplotlib.pyplot as plt # 用于数据可视化
import numpy as np

# list的数据如下：
# [['201018~201024'], [3357, 506]]
# [['201011~201017'], [3603, 434]]
    
for t in list[0]:
    plt.title(t, loc ='left') 
    
n = 2 
x = np.arange(2) # x轴2个数据
    
index_x = ['phone time', 'attention time']
plt.xticks(x,index_x) # 设置了x轴显示的文本标签
    
y = list[1]
plt.yticks([]) # 关闭y轴刻度
    
ax=plt.gca() # 调用坐标轴来绘图，这里获取当前的坐标轴
ax.spines['right'].set_color('none') #设置图片的右边框和上边框为不显示
ax.spines['top'].set_color('none')
    
# 显示y轴数值
    
for a, b in zip(x, y):
    plt.text(a, b + 50, '%d'%b+'min') # b+50表示在y值上方50处标注文字说明
    
plt.bar(x, height = y, color=['r','g'],alpha=0.5) # 设置不同颜色
plt.show()
```

> d2 解决emoji问题（未果）

导入emoji，在图片上显示胜利emoji和失败emoji，参考[Python实现emoji表情](https://pypi.org/project/emoji/)
使用pip3安装emoji：
```
miodeMacBook-Pro:~ mio$ pip3 install emoji
Collecting emoji
  Downloading emoji-0.6.0.tar.gz (51 kB)
     |████████████████████████████████| 51 kB 35 kB/s 
Using legacy 'setup.py install' for emoji, since package 'wheel' is not installed.
Installing collected packages: emoji
    Running setup.py install for emoji ... done
Successfully installed emoji-0.6.0
```

尝试使用一下：
```
import emoji
a = emoji.emojize('test1:yellow_heart:', use_aliases=True)
b = emoji.emojize('test2:imp:', use_aliases=True)
c = emoji.emojize('test3:heartbeat:', use_aliases=True)
d = emoji.emojize('test4:cry:', use_aliases=True)
print(a, b, c, d)
```
输出：
`test1💛 test2👿 test3💓 test4😢 `
成功

选择两个符合，放入代码中，代码可以参考[emoji代码](http://www.oicqzone.com/tool/emoji/)
代码中空格用下划线_连接
- 在手机屏幕使用时间大于专注时间时，显示💀 :skull:
- 在专注时间大于手机屏幕使用时间时，显示💗 :sparkling heart:

```
import emoji

win = emoji.emojize('you win :sparkling_heart::sparkling_heart::sparkling_heart:', use_aliases=True)  
fail = emoji.emojize('you fail :skull::skull::skull:', use_aliases=True)

if t2 > t3:
    plt.title(t + fail) 
else:
    plt.title(t + win)
```

输出时，发现emoji和中文一样显示为方块，matplotli对字符的支持这么差？看看有没有解决方法，没有的话准备换一个可视化模块了。

通过查询发现，中文字符显示可以通过设置font格式实现，emoji应当也可。

`plt.rcParams["font.family"] = 'Arial Unicode MS'`

网上说可以用“segoe ui emoji”，下载安装了字体，也在matlpotlib的ttf文件夹里面安装了，依然不支持emjio，试试看查询matplotlib已有的库有的字体：

```
import matplotlib    
print(matplotlib.matplotlib_fname())
```
地址在/Users/mio/Library/Python/3.8/lib/python/site-packages/matplotlib/mpl-data/matplotlibrc
在这个文件夹的ttf文件里面有字体库，随机试了几种，不行。

重新梳理问题：
- 诉求：在matplotlib中显示emoji
- 方案：寻找可以显示emoji且matplotlib支持的字体
   - 查阅资料发现，支持emoji的字符是utf-8mb4字符，符合这种编码的可以显示emoji，utf8mb4兼容utf8，也就是找字体解决，问题就可以解决
      - 找字体
      - 设置官方文档让它支持utf8mb4？
      
经过提问，大妈解答[issue54](https://gitlab.com/101camp/11py/tasks/-/issues/54),提到的：
- [How I got Matplotlib to plot Apple Color Emojis](https://towardsdatascience.com/how-i-got-matplotlib-to-plot-apple-color-emojis-c983767b39e0)
- [在Matplotlib中使用表情符号](https://newschematic.org/blog/using-emoji-in-matplotlib/)

我先尝试Apple_Color_Emoji，尝试下载，链接有点难找，照理说我的mac系统应当自带字体？
果然，在字体册中找到了它。既然有字体，为啥不能显示？

我做了如下操作：
```
from matplotlib.font_manager import FontProperties
prop = FontProperties(fname='/System/Library/Fonts/Apple\ Color\ Emoji.ttc')
```
指定了字体地址，依然无法显示。我尝试把它复制到matplotlib的字库里，复制完成后，还是无法显示。根据资料，可以考虑：
- ttc文件格式影响了显示，用第三方模块Mplcairo试试。
   - 先`pip3 install pycairo`和`pip3 install pycairo`，pycairo安装失败，似乎有点麻烦
   - 找到了ttf字体，但是依然无法显示
- 博客中的Symbola font最起码可以显示无颜色的wmoji，这是默认字体，可以试试
   - 尝试fontname='Symbola',不显示
- 清理字体库的缓存
   - 未找到缓存处

参考[在Matplotlib中使用表情符号](https://newschematic.org/blog/using-emoji-in-matplotlib/)，我尝试了文中的代码，博主的问题是emoji没有颜色，我用了他的源代码，发现我的显示是没有图片的，同样有字体，但是没有显示，这是个什么道理？
也许问题出在matplotlib的字体缓存上？
整理一下现在的情况：
- emoji可以显示在终端
- emoji字体存在
- matplotlib不显示emoji

再次回到字体库，我尝试在源代码中，删除代码字符串，直接打emoji，如
`plt.title(t + '😁' ) `可以显示无颜色的emoji，不过能显示的表情有限，那么问题难道出在emoji的表达上？正好上门的[How I got Matplotlib to plot Apple Color Emojis](https://towardsdatascience.com/how-i-got-matplotlib-to-plot-apple-color-emojis-c983767b39e0)blog有同样的问题，跟着上面的blog试试看。
发现blog中的代码，删除定义emoji的部分，可以显示无颜色的emoji，反而是加了定义字体的代码之后不显示任何。
在这篇blog中，回到了之前中断的Mplcairo，使用这个模块，会支持TTC文件,但是得先安装cairo，它支持了颜色的显示。安装它呢需要先安装homebrew，不然就得在gihub花不知道多少时间下载安装包。

在homebrew官网安装遇到443连接失败问题，参考[homebrew443问题解决方案](https://www.jianshu.com/p/ed35ce1981e8) ,未解决，暂缓此问题

> d3 新增总趋势折线图

```
# 总趋势折线图
# 将之前的数据append到折线图需要的数据中，sx，sy1，sy2
n = len(sx)
nx = range(0, n)
print(n)
plt.title('总趋势')
plt.rcParams['font.sans-serif'] = ['SimHei']  # 显示汉字
plt.yticks([]) # 关闭y轴刻度
plt.xticks([])
plt.plot(nx, sy1, alpha=0.6)  # 绘制折线图，添加数据点，设置点的大小
plt.plot(nx, sy2, alpha=0.6)
plt.legend(['phone time', 'attention time'])  # 设置折线名称
plt.show() 
```
发现一个问题，显示时候，是按周的倒序来排列的，希望改成时间顺序来生成，也就是需要从列表最末尾取值，顺序颠倒：使用reversed()方法会将列表逆序的结果存储到迭代器里面，这种方式不会改变原来的列表: 
```
x = [1, 2, 3, 4, 5]
x.reverse()
print(x) # 输出结果是[5, 4, 3, 2, 1]
``` 
但是我遇到了问题;
```
y1 = sy1.reverse()
print(y1) 
```
打印出来的是none，后来发现，只需要`sy1.reverse()`，然后继续使用sy1，不需要赋值

再增加x轴文本标签，搞定。
```
# 总趋势折线图

n = len(sx)
nx = range(0, n)
sy1.reverse()
sy2.reverse()
sx.reverse() # [['201011~201017'], ['201018~201024'], ['201025~201031']]

plt.title('总趋势')
plt.rcParams['font.sans-serif'] = ['SimHei']  # 显示汉字

ax=plt.gca() # 调用坐标轴来绘图，这里获取当前的坐标轴
ax.spines['right'].set_color('none') #设置边框为不显示
ax.spines['top'].set_color('none')

plt.yticks([]) # 关闭y轴刻度

plt.plot(nx, sy1, alpha=0.6)  # 绘制折线图
for a1, b1 in zip(nx, sy1):
    plt.text(a1, b1 + 50, '%d'%b1+'min') 
plt.plot(nx, sy2, alpha=0.6)
for a2, b2 in zip(nx, sy2):
    plt.text(a2, b2 + 50, '%d'%b2+'min') 
plt.legend(['phone time', 'attention time'])  # 设置折线名称

label = []
for li in sx:
    lsx = str(li).replace('[','').replace(']','')
    label.append(lsx)
# print(label) # ["'201011~201017'", "'201018~201024'", "'201025~201031'"]
plt.xticks(nx, label)
plt.show() 
```













                                         








      














