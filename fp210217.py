# coding = utf - 8

import requests # 用于获取api数据
import re # 用于正则匹配，获取特定数据

import matplotlib.pyplot as plt # 用于数据可视化
import numpy as np

import pprint
pp = pprint.PrettyPrinter(indent=4)

import emoji

with open('token.txt') as f:
    pt = f.read() 
    
r = requests.get('https://gitlab.com/api/v4/projects/21987874/issues/1/notes', headers = {"PRIVATE-TOKEN": pt})
# print(r.status_code) # response200，连接成功

describe = r.json()
# pp.pprint(describe)

infomation = []
for i in describe:
    infomation.append(i['body'])
    
newinfomation = [f for f in infomation if 'changed the description' not in f]  
# pp.pprint(newinfomation)

num = len(newinfomation)
# print(num)

# 每周使用时间

sx = [] # 用于总趋势图获取数据
sy1 = []
sy2 = []

for i in newinfomation[0: num]:
    # print(i)
    
    day = re.compile('## (.+)：')
    result1 = day.findall(i)
    sx.append(result1)
    
    hour = re.compile('共(.+)小时') 
    result2 = hour.findall(i)
    
    mintues = re.compile('小时(.+)分钟') 
    result3 = mintues.findall(i)
    
    t1 = result2 + result3
    
    t2 = int(t1[0]) * 60 + int(t1[2])
    t3 = int(t1[1]) * 60 + int(t1[3])
    
    sy1.append(t2)
    sy2.append(t3)
    
    timelist =[t2, t3]
    list = [result1, timelist]
    # print(list)
    # [['201018~201024'], [3357, 506]]
    # [['201011~201017'], [3603, 434]]
     
    
# 总趋势折线图

n = len(sx)
nx = range(0, n)
sy1.reverse()
sy2.reverse()
sx.reverse() # [['201011~201017'], ['201018~201024'], ['201025~201031']]

plt.title('总趋势')
plt.rcParams['font.sans-serif'] = ['SimHei']  # 显示汉字

ax=plt.gca() # 调用坐标轴来绘图，这里获取当前的坐标轴
ax.spines['right'].set_color('none') #设置边框为不显示
ax.spines['top'].set_color('none')

plt.yticks([]) # 关闭y轴刻度

plt.plot(nx, sy1, alpha=0.6)  # 绘制折线图
for a1, b1 in zip(nx, sy1):
    plt.text(a1, b1 + 50, '%d'%b1+'min') 
plt.plot(nx, sy2, alpha=0.6)
for a2, b2 in zip(nx, sy2):
    plt.text(a2, b2 + 50, '%d'%b2+'min') 
plt.legend(['phone time', 'attention time'])  # 设置折线名称

label = []
for li in sx:
    lsx = str(li).replace('[','').replace(']','')
    label.append(lsx)
# print(label) 
plt.xticks(nx, label)
plt.show() 




# 负债情况显示

sumph = sum(sy1)
sumatt = sum(sy2)
s = sumph - sumatt # 手机时间减去专注时间

if s > 0 :
    plt.title('专注时间负债: %s min' %s, fontsize = 28)
elif s == 0 :
    plt.title('Amazing!打平了!', fontsize = 28)
else: 
    plt.title('手机使用时间盈余: %s min' %s, fontsize = 28)


x = [sumph, sumatt] 
n = 2 
x = np.arange(2) # x轴2个数据    
index_x = ['phone time', 'attention time']
plt.xticks(x,index_x)

y = list[1]
plt.yticks([]) # 关闭y轴刻度
    
ax=plt.gca() # 调用坐标轴来绘图，这里获取当前的坐标轴
ax.spines['right'].set_color('none') #设置边框为不显示
ax.spines['top'].set_color('none')

plt.bar(x, y , color=['r','g'],alpha=0.5 )
plt.show()   


    
    
